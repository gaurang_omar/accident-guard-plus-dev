import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import {NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-convert-to-proposal',
  templateUrl: './convert-to-proposal.component.html',
  styleUrls: ['./convert-to-proposal.component.scss']
})
export class ConvertToProposalComponent implements OnInit {
  detailsCompleted: Object = {
    customerDetails: false,
    memberDetails: false,
    nomineeDetails: false,
    additionalPolicyDetails: false,
    additionalDetails: true,
    documentUpload: false
  };
  declarationModalRef: BsModalRef;
  // config = { class: 'theme-modal' };
  config : NgbModalOptions = {
    backdrop : 'static',
    keyboard : false
  };
  agentDeclaration: Boolean = false;
  go_greenDeclaration: Boolean = false;
  personDeclaration: Boolean = false;
  docType :string = '';
  constructor(private router: Router,
    private modalService: BsModalService) { }

  ngOnInit(): void {
    this.config["class"] = "theme-modal";
  }

  reviewQuote() {
    this.routeBack();
  }

  routeBack() {
    this.router.navigate(['accident-guard-plus/previewQuote']);
  }

  declareQuote(declaration_template: TemplateRef<any>, decType) {
    this.docType = decType;
    this.declarationModalRef = this.modalService.show(declaration_template, this.config);
  }

  addDetails(detailsType) {
    let url = 'accident-guard-plus/'+detailsType;
    this.router.navigate([url]);
  }

  route() {
    this.router.navigate(['accident-guard-plus/previewProposal']);
  }

}
