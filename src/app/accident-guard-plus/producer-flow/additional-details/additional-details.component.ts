import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-additional-details',
  templateUrl: './additional-details.component.html',
  styleUrls: ['./additional-details.component.scss']
})
export class AdditionalDetailsComponent implements OnInit {

  additionalDetailsForm : FormGroup;

  constructor(private _router : Router,
              private _formBuilder : FormBuilder) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm(){
    this.additionalDetailsForm = this._formBuilder.group({
      customerPanNumber : [''],
      customerGstinNo : ['']
    });
  }

  routeBack(){
    this._router.navigate(['accident-guard-plus/additionalPolicyDetails']);
  }

  route(){
    this._router.navigate(['accident-guard-plus/documentUpload']);
  }

  routeBackToProposal(){
    this._router.navigate(['accident-guard-plus/convertToProposal']);  
  }

}
