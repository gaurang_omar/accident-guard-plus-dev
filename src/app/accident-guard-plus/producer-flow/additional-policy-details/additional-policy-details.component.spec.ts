import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalPolicyDetailsComponent } from './additional-policy-details.component';

describe('AdditionalPolicyDetailsComponent', () => {
  let component: AdditionalPolicyDetailsComponent;
  let fixture: ComponentFixture<AdditionalPolicyDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditionalPolicyDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalPolicyDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
