import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPremiumsComponent } from './all-premiums.component';

describe('AllPremiumsComponent', () => {
  let component: AllPremiumsComponent;
  let fixture: ComponentFixture<AllPremiumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllPremiumsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllPremiumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
