import { AfterViewInit, Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EventManager } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-all-premiums',
  templateUrl: './all-premiums.component.html',
  styleUrls: ['./all-premiums.component.scss']
})
export class AllPremiumsComponent implements OnInit, AfterViewInit {

  allPremiumsForm : FormGroup;
  dropDownArr = [];
  contactModalRef: BsModalRef;
  successModalRef: BsModalRef;
  config: any = {
    backdrop: 'static',
    keyboard: false,
    class: 'theme-modal'
  };
  savePremium: boolean;
  sharePremium: boolean;
  invalidMsg: Boolean = false;
  contactDetailForm: FormGroup;
  salutationArr = ['Mr', 'Mrs', 'Ms'];
  months: Object = {
    'jan': '01',
    'feb': '02',
    'mar': '03',
    'apr': '04',
    'may': '05',
    'jun': '06',
    'jul': '07',
    'aug': '08',
    'sep': '09',
    'oct': '10',
    'nov': '11',
    'dec': '12'
  };
  @ViewChild("policyStartDate") policyStartDate: ElementRef;
  premiumBreakupModalRef: BsModalRef;
  policyEndDate : any;

  constructor(private _router : Router,
              private _formBuilder : FormBuilder,
              private modalService: BsModalService,
              private eventManager: EventManager) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.preFormatDates(this.policyStartDate.nativeElement);
  }

  initializeForm(){
    this.allPremiumsForm = this._formBuilder.group({
      sumInsured : [null],
      policyStartDate : [''],
      policyEndDate : [''],
      tenure : ['']
    });
    this.contactDetailForm = this._formBuilder.group({
      salutation : [null],
      firstName: [''],
      middleName: [''],
      lastName: [''],
      email: [''],
      mobile: ['']
    });
  }

  contactDetailModal(contact_detail_template: TemplateRef<any>, premiumType) {
    this.savePremium = premiumType == 'save' ? true : false;
    this.sharePremium = premiumType == 'share' ? true : false;
    this.contactModalRef = this.modalService.show(contact_detail_template, this.config);
  }

  shareQuote(success_template: TemplateRef<any>) {
    this.contactModalRef.hide();
    this.successModalRef = this.modalService.show(success_template, this.config);
  }

  isNumberKey(evt: any) {
    if (
      (evt.key >= "0" && evt.key <= "9") ||
      evt.key == "Backspace" ||
      evt.key == "Delete" ||
      evt.key == "ArrowLeft" ||
      evt.key == "ArrowRight"
    ) {
      return true;
    }
    return false;
  }

  formatName(evt: any) {
    var regex = new RegExp("^[a-zA-Z \s]+$");
    var str = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
    if (regex.test(str)) {
      return true;
    } else {
      evt.preventDefault();
      return false;
    }
  }

  formatEmail(evt: any) {
    var regex = /[A-Z0-9a-z@\._-]/;
    var str = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
    if (regex.test(str)) {
      return true;
    } else {
      evt.preventDefault();
      return false;
    }
  }

  checkValue(str, max) {
    if (str.charAt(0) !== '0' || str == '00') {
      var num = parseInt(str);
      if (isNaN(num) || num <= 0 || num > max) num = 1;
      str = num > parseInt(max.toString().charAt(0))
        && num.toString().length == 1 ? '0' + num : num.toString();
    };
    return str;
  }

  preFormatDates(htmlElement) {
    this.eventManager.addEventListener(htmlElement, 'keydown', (e) => {
      var input = htmlElement.value;
      if (/[\-]/.test(input)) return;
      var key = e.keyCode || e.charCode;

      if (key == 8 || key == 46)
        return false;

      if (/\D\/$/.test(input)) input = input.substr(0, input.length - 1);
      var values = input.split('/').map(function (v) {
        return v.replace(/\D/g, '')
      });
      if (values[0]) values[0] = this.checkValue(values[0], 31);
      if (values[1]) values[1] = this.checkValue(values[1], 12);
      var output = values.map(function (v, i) {
        return v.length == 2 && i < 2 ? v + '/' : v;
      });
      htmlElement.value = output.join('').substr(0, 10);
    });
  }

  formatLikeExcel(dateControl) {
    var values = this.allPremiumsForm.get(dateControl).value;

    var arr = values.split("-");
    // console.log("parsed value",arr,arr.length,arr[1],parseInt(arr[1]));
    if (arr.length > 1) {
      arr[1] = parseInt(arr[1]) ? parseInt(arr[1]) : this.months[arr[1].toLocaleLowerCase()];
      arr[2] = parseInt(arr[2]) ? parseInt(arr[2]) : '';
      var finalValue = arr.join('/');
      // htmlElement.value = finalValue;
      this.allPremiumsForm.get(dateControl).setValue(finalValue);
    }
    else{
      var arr = values.split("/");
      if(arr.length > 1){    
        arr[1] = parseInt(arr[1]) ? parseInt(arr[1]) : this.months[arr[1].toLocaleLowerCase()];
        arr[2] = parseInt(arr[2]) ? parseInt(arr[2]) : '';
        var finalValue = arr.join('/');
        this.allPremiumsForm.get(dateControl).setValue(finalValue);
      }
    }
    this.policyEndDate = '11/12/2020';
  }

  openPremiumBreakupModal(premium_breakup_modal_template: TemplateRef<any>) {
    this.premiumBreakupModalRef = this.modalService.show(premium_breakup_modal_template, this.config);
  }

  routeBack(){
    this._router.navigate(['accident-guard-plus/memberDetails']);
  }

  route(){
    this.successModalRef.hide();
    this._router.navigate(['accident-guard-plus/previewQuote']);
  }

  viewQuotes(){
    this.successModalRef.hide();
    this._router.navigate(['accident-guard-plus/quoteListing']);
  }

}
