import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quote-information',
  templateUrl: './quote-information.component.html',
  styleUrls: ['./quote-information.component.scss']
})
export class QuoteInformationComponent implements OnInit {

  quoteInformationForm : FormGroup;
  digits : Object = {
    '4' : 1000,
    // '5' : 10000,
    '6' : 100000,
    // '7' : 1000000,
    '8' : 10000000
  };
  incomeAmountInWords : String = '';
  dropDownArr = [];

  constructor(private _router : Router,
              private _formBuilder : FormBuilder) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm(){
    this.quoteInformationForm = this._formBuilder.group({
      cityPincode : [''],
      productVariant : [''],
      occupation : [null],
      annualIncome : [''],
      gstinNumber : ['']
    })
  }

  annualIncomeEnter(){
    this.incomeAmountInWords = this.incomeInWords(this.quoteInformationForm.get('annualIncome').value);
    console.log("inciome",this.incomeAmountInWords);
    
  }

  incomeInWords(incomeAmount) {
    var sglDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"],
      dblDigit = ["Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"],
      tensPlace = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"],
      handle_tens = function(dgt, prevDgt) {
        return 0 == dgt ? "" : " " + (1 == dgt ? dblDigit[prevDgt] : tensPlace[dgt])
      },
      handle_utlc = function(dgt, nxtDgt, denom) {
        return (0 != dgt && 1 != nxtDgt ? " " + sglDigit[dgt] : "") + (0 != nxtDgt || dgt > 0 ? " " + denom : "")
      };
  
    var str = "",
      digitIdx = 0,
      digit = 0,
      nxtDigit = 0,
      words = [];
    if (incomeAmount += "", isNaN(parseInt(incomeAmount))) str = "";
    else if (parseInt(incomeAmount) > 0 && incomeAmount.length <= 10) {
      for (digitIdx = incomeAmount.length - 1; digitIdx >= 0; digitIdx--) switch (digit = incomeAmount[digitIdx] - 0, nxtDigit = digitIdx > 0 ? incomeAmount[digitIdx - 1] - 0 : 0, incomeAmount.length - digitIdx - 1) {
        case 0:
          words.push(handle_utlc(digit, nxtDigit, ""));
          break;
        case 1:
          words.push(handle_tens(digit, incomeAmount[digitIdx + 1]));
          break;
        case 2:
          words.push(0 != digit ? " " + sglDigit[digit] + " Hundred" + (0 != incomeAmount[digitIdx + 1] && 0 != incomeAmount[digitIdx + 2] ? " and" : "") : "");
          break;
        case 3:
          words.push(handle_utlc(digit, nxtDigit, "Thousand"));
          break;
        case 4:
          words.push(handle_tens(digit, incomeAmount[digitIdx + 1]));
          break;
        case 5:
          words.push(handle_utlc(digit, nxtDigit, "Lakh"));
          break;
        case 6:
          words.push(handle_tens(digit, incomeAmount[digitIdx + 1]));
          break;
        case 7:
          words.push(handle_utlc(digit, nxtDigit, "Crore"));
          break;
        case 8:
          words.push(handle_tens(digit, incomeAmount[digitIdx + 1]));
          break;
        case 9:
          words.push(0 != digit ? " " + sglDigit[digit] + " Hundred" + (0 != incomeAmount[digitIdx + 1] || 0 != incomeAmount[digitIdx + 2] ? " and" : " Crore") : "")
      }
      str = words.reverse().join("")
    } else str = "";
    return str  
  }

  isNumberKey(evt: any) {
    if (
      (evt.key >= "0" && evt.key <= "9") ||
      evt.key == "Backspace" ||
      evt.key == "Delete" ||
      evt.key == "ArrowLeft" ||
      evt.key == "ArrowRight"
    ) {
      return true;
    }
    return false;
  }

  route(){
    this._router.navigate(['accident-guard-plus/memberDetails']);
  }

}
