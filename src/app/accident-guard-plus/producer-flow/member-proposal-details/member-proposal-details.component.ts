import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { EventManager } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-member-proposal-details',
  templateUrl: './member-proposal-details.component.html',
  styleUrls: ['./member-proposal-details.component.scss']
})
export class MemberProposalDetailsComponent implements OnInit {

  memberDetailsForm : FormGroup;
  months: Object = {
    'jan': '01',
    'feb': '02',
    'mar': '03',
    'apr': '04',
    'may': '05',
    'jun': '06',
    'jul': '07',
    'aug': '08',
    'sep': '09',
    'oct': '10',
    'nov': '11',
    'dec': '12'
  };
  dropDownArr = [];

  constructor(private _router : Router,
              private _formBuilder : FormBuilder,
              private eventManager: EventManager,
              private translate: TranslateService) { }

  ngOnInit(): void {
    this.initializeForm();
    console.log("INIT FORM:::",this.memberDetailsForm);
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    const memberArr = this.memberDetailsForm.get('memberArr') as FormArray;
    let arrLength  = memberArr.length;
    let dateCombArr = this.getDateCombinationArrSF(arrLength);
    dateCombArr.forEach(ele => {
      this.preFormatDates(document.getElementById(ele));
    });
  }

  initializeForm(){
    this.memberDetailsForm = this._formBuilder.group({
      memberArr : this._formBuilder.array([this.addMemberGroup(this.translate.instant('memberDetails.proposal.selfDetails')),
                                           this.addMemberGroup(this.translate.instant('memberDetails.proposal.spouseDetails')),
                                           this.addMemberGroup(this.translate.instant('memberDetails.proposal.child_1_Details')),
                                           this.addMemberGroup(this.translate.instant('memberDetails.proposal.child_2_Details'))])
    })
  }

  addMemberGroup(member):FormGroup{
    return this._formBuilder.group({
      memberType : [member],
      title : [null],
      firstName : [''],
      middleName : [''],
      lastName : [''],
      dateOfBirth : [''],
      gender : [null],
      maritalStatus : [null],
      relationship : [null],
      historyResp : [false]
    });
  }

  isNumberKey(evt: any) {
    if (
      (evt.key >= "0" && evt.key <= "9") ||
      evt.key == "Backspace" ||
      evt.key == "Delete" ||
      evt.key == "ArrowLeft" ||
      evt.key == "ArrowRight"
    ) {
      return true;
    }
    return false;
  }

  formatName(evt: any) {
    var regex = new RegExp("^[a-zA-Z \s]+$");
    var str = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
    if (regex.test(str)) {
      return true;
    } else {
      evt.preventDefault();
      return false;
    }
  }

  formatEmail(evt: any) {
    var regex = /[A-Z0-9a-z@\._-]/;
    var str = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
    if (regex.test(str)) {
      return true;
    } else {
      evt.preventDefault();
      return false;
    }
  }

  checkValue(str, max) {
    if (str.charAt(0) !== '0' || str == '00') {
      var num = parseInt(str);
      if (isNaN(num) || num <= 0 || num > max) num = 1;
      str = num > parseInt(max.toString().charAt(0))
        && num.toString().length == 1 ? '0' + num : num.toString();
    };
    return str;
  }

  preFormatDates(htmlElement) {
    this.eventManager.addEventListener(htmlElement, 'keydown', (e) => {
      var input = htmlElement.value;
      if (/[\-]/.test(input)) return;
      var key = e.keyCode || e.charCode;

      if (key == 8 || key == 46)
        return false;

      if (/\D\/$/.test(input)) input = input.substr(0, input.length - 1);
      var values = input.split('/').map(function (v) {
        return v.replace(/\D/g, '')
      });
      if (values[0]) values[0] = this.checkValue(values[0], 31);
      if (values[1]) values[1] = this.checkValue(values[1], 12);
      var output = values.map(function (v, i) {
        return v.length == 2 && i < 2 ? v + '/' : v;
      });
      htmlElement.value = output.join('').substr(0, 10);
    });
  }

  formatLikeExcel(member,dateControl) {
    var values = member.get(dateControl).value;

    var arr = values.split("-");
    // console.log("parsed value",arr,arr.length,arr[1],parseInt(arr[1]));
    if (arr.length > 1) {
      arr[1] = parseInt(arr[1]) ? parseInt(arr[1]) : this.months[arr[1].toLocaleLowerCase()];
      arr[2] = parseInt(arr[2]) ? parseInt(arr[2]) : '';
      var finalValue = arr.join('/');
      // htmlElement.value = finalValue;
      member.get(dateControl).setValue(finalValue);
    }
    else{
      var arr = values.split("/");
      if(arr.length > 1){    
        arr[1] = parseInt(arr[1]) ? parseInt(arr[1]) : this.months[arr[1].toLocaleLowerCase()];
        arr[2] = parseInt(arr[2]) ? parseInt(arr[2]) : '';
        var finalValue = arr.join('/');
        member.get(dateControl).setValue(finalValue);
      }
    }
  }

  getDateCombinationArrSF(length) {
    console.log("IN LOOP ::::::", ":::::::::", length);
    let dateCombArr = [];
    for (var i = 0; i < length; i++) {
      dateCombArr.push('dateOfBirth' + i);
    }
    console.log("COMBINATION ARR::::", dateCombArr);
    return dateCombArr;
  }


  routeBack(){
    this._router.navigate(['accident-guard-plus/customerDetails']);
  }

  route(){
    this._router.navigate(['accident-guard-plus/nomineeDetails']);
  }

  routeBackToProposal(){
    this._router.navigate(['accident-guard-plus/convertToProposal']);  
  }

}
