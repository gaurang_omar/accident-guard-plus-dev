import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberProposalDetailsComponent } from './member-proposal-details.component';

describe('MemberProposalDetailsComponent', () => {
  let component: MemberProposalDetailsComponent;
  let fixture: ComponentFixture<MemberProposalDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberProposalDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberProposalDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
