import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-review-proposal',
  templateUrl: './review-proposal.component.html',
  styleUrls: ['./review-proposal.component.scss']
})
export class ReviewProposalComponent implements OnInit {

  dropDownArr = [];
  contactModalRef: BsModalRef;
  successModalRef: BsModalRef;
  config: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  savePremium: boolean;
  sharePremium: boolean;
  invalidMsg: Boolean = false;
  contactDetailForm: FormGroup;
  salutationArr = ['Mr', 'Mrs', 'Ms'];
  isProposalFinalized : Boolean = false;
  premiumBreakupModalRef: BsModalRef;

  constructor(private router : Router,
    private _formBuilder : FormBuilder,
    private modalService: BsModalService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.config["class"] = "theme-modal";
    this.initializeForm();
  }

  initializeForm(){
    this.contactDetailForm = this._formBuilder.group({
      salutation : [null],
      firstName: [''],
      middleName: [''],
      lastName: [''],
      email: [''],
      mobile: [''],
      pincode: [null]
    });
  }

  contactDetailModal(contact_detail_template: TemplateRef<any>, premiumType) {
    this.savePremium = premiumType == 'save' ? true : false;
    this.sharePremium = premiumType == 'share' ? true : false;
    this.contactModalRef = this.modalService.show(contact_detail_template, this.config);
  }

  shareQuote(success_template: TemplateRef<any>) {
    this.contactModalRef.hide();
    this.successModalRef = this.modalService.show(success_template, this.config);
  }

  isNumberKey(evt: any) {
    if (
      (evt.key >= "0" && evt.key <= "9") ||
      evt.key == "Backspace" ||
      evt.key == "Delete" ||
      evt.key == "ArrowLeft" ||
      evt.key == "ArrowRight"
    ) {
      return true;
    }
    return false;
  }

  formatName(evt: any) {
    var regex = new RegExp("^[a-zA-Z \s]+$");
    var str = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
    if (regex.test(str)) {
      return true;
    } else {
      evt.preventDefault();
      return false;
    }
  }

  formatEmail(evt: any) {
    var regex = /[A-Z0-9a-z@\._-]/;
    var str = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
    if (regex.test(str)) {
      return true;
    } else {
      evt.preventDefault();
      return false;
    }
  }

  routeBack(){
    this.router.navigate(['accident-guard-plus/convertToProposal']);
  }

  route(){
    this.successModalRef.hide();
  }

  finalizeProposal(){
    this.toastr.success('Policy Generated Successfully. Your Policy Number is AXCVBADA.','',{closeButton:true});
    this.isProposalFinalized = true;
  }

  openPremiumBreakupModal(premium_breakup_modal_template: TemplateRef<any>) {
    this.premiumBreakupModalRef = this.modalService.show(premium_breakup_modal_template, this.config);
  }


}
