import { Component, OnInit, TemplateRef } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
// import { TranslateService } from '@ngx-translate/core';
import { IDropdownSettings } from "ng-multiselect-dropdown";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";

@Component({
  selector: 'app-quote-listing',
  templateUrl: './quote-listing.component.html',
  styleUrls: ['./quote-listing.component.scss']
})
export class QuoteListingComponent implements OnInit {

  sortFilterRef: BsModalRef;
  sortFilterForm: FormGroup;
  config: NgbModalOptions = {
    backdrop: "static",
    keyboard: false,
  };
  quoteListForm: FormGroup;
  page: number = 1;
  dropdownListChannel = [];
  dropdownList = [];
  selectedItemsChannel = [];
  selectedItems = [];
  dropdownSettings: IDropdownSettings = {};
  quoteListing = [];
  quoteListingArray = [];
  commonStatus: string = "All policies";
  searchText: string;
  selectedFilterParameters: string;
  filterFromDate: string;
  filterToDate: string;
  isUnderWriter: boolean = false;
  isFilterApplied: boolean = false;
  uwLocationList = [];
  array = [1,2,3,4,5,6];
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
  ) {
    // this.getDropDowns();
    this.dropdownSettings = {
      singleSelection: false,
      idField: "item_id",
      textField: "item_text",
      // selectAllText: 'Select All',
      // unSelectAllText: 'UnSelect All',
      // itemsShowLimit: 1,
    };
  }

  ngOnInit() {
    this.config["class"] = "theme-modal";
  }

  // async getUnderWriterLocationList() {
  //   await this.quoteService
  //     .getUnderWriterLocationList()
  //     ?.then((list: any) => {
  //       this.uwLocationList = list?.data;
  //     })
  //     ?.catch((e) => console.log(e));
  // }

  // async getDropDowns() {
  //   const channelDropDownList: any =
  //     await this.quoteService.getChannelDropDown();
  //   this.dropdownListChannel = channelDropDownList?.data?.data?.map(
  //     (channel, index) => ({
  //       item_id: index + 1,
  //       item_text: channel.txt_channel_name,
  //     })
  //   );
  //   this.dropdownList = [sessionStorage.getItem("location")];
  // }

  initSortFilterForm() {
    this.sortFilterForm = this.formBuilder.group({
      dateSortType: [""],
      snoSortType: [""],
      startFromDate: [""],
      endToDate: [""],
    });
  }

  // async getQuoteListingData(filterWithoutData = false) {
  //   await this.quoteService
  //     .getQuoteList({
  //       fields: "quote.*",
  //       order: "u_ts desc",
  //       ...(this.isUnderWriter && {
  //         assignedToTeam: 1,
  //         filter: this.uwAssignedGroup,
  //         created_by: null,
  //       }),
  //       ...(!this.isUnderWriter && {
  //         created_by: sessionStorage.getItem("email"),
  //       }),
  //     })
  //     .then((list: any) => {
  //       if (this.isUnderWriter && this.uwLocationList?.length) {
  //         this.quoteListing = list?.data?.filter((q) => {
  //           return this.uwLocationList?.some(
  //             (el) => el.location === q?.data?.search?.location
  //           );
  //         });
  //       } else {
  //         this.quoteListing = list?.data;
  //       }
  //       this.quoteListingArray = this.quoteListing;
  //       this.commonStatus = "All policies";
  //       this.computeQuoteStatus();
  //       if (filterWithoutData || this.isFilterApplied) {
  //         this.filterQuoteListingArray();
  //       }
  //     })
  //     .catch((e) => console.log(e));
  // }

  initializeForm() {
    this.quoteListForm = this.formBuilder.group({
      searchInp: [""],
    });
  }

  routeBack() {
    this.router.navigate(["/accident-guard-plus/allPremiums"]);
  }

  viewQuote({ quote_id }) {
    // this.router.navigate([`/accident-guard-plus/quote/preview-quote/${quote_id}`]);
  }

  /**
   * Function to open sort filter modal
   * @param sort_filter_template : template for sort filter modal
   */
  sortFilter(sort_filter_template: TemplateRef<any>) {
    this.sortFilterRef = this.modalService.show(
      sort_filter_template,
      this.config
    );
  }
  // computeQuoteStatus() {
  //   this.quoteListing.forEach((item) => {
  //     if (this.isUnderWriter) {
  //       if (
  //         !item.policy_no &&
  //         item.quote.data &&
  //         item.quote.data.query_opted &&
  //         item.quote.data.query_option_yn == "yes"
  //       ) {
  //         item.quote_status = QuoteListingStatus.QUERIED;
  //       } else {
  //         item.quote_status = QuoteListingStatus.PENDING;
  //       }
  //     } else if (!this.isUnderWriter) {
  //       if (item.policy_no) {
  //         item.quote_status = QuoteListingStatus.APPROVED_AND_POLICY_GENERATED;
  //       } else if (
  //         !item.policy_no &&
  //         item.quote.data &&
  //         item.quote.data.query_opted &&
  //         item.quote.data.query_option_yn == "yes"
  //       ) {
  //         item.quote_status = QuoteListingStatus.QUERIED;
  //       } else if (
  //         !item.policy_no &&
  //         !item.quote_no &&
  //         item.data &&
  //         item.data.product_nstp_status &&
  //         item.data.product_nstp_status == "approved"
  //       ) {
  //         item.quote_status = QuoteListingStatus.UW_APPROVED;
  //       } else if (
  //         !item.policy_no &&
  //         item.quote_no &&
  //         item.data &&
  //         item.data.product_nstp_status &&
  //         item.data.product_nstp_status == "approved"
  //       ) {
  //         item.quote_status = QuoteListingStatus.PENDING;
  //       } else if (
  //         !item.policy_no &&
  //         item.data &&
  //         item.data.product_nstp_status &&
  //         item.data.product_nstp_status == "rejected"
  //       ) {
  //         item.quote_status = QuoteListingStatus.UW_REJECTED;
  //       } else if (
  //         !item.policy_no &&
  //         item.quote.data &&
  //         !item.quote.data.query_opted &&
  //         !item.data.product_nstp_status &&
  //         item.created_by == item.assigned_to
  //       ) {
  //         item.quote_status = QuoteListingStatus.PENDING;
  //       } else if (
  //         !item.policy_no &&
  //         item.data &&
  //         item.data.product_nstp_status &&
  //         item.data.product_nstp_status == "Referral"
  //       ) {
  //         item.quote_status = QuoteListingStatus.UW_REFERRAL;
  //       } else if (
  //         (!item.policy_no &&
  //           item.quote.data &&
  //           !item.quote.data.query_opted &&
  //           !item.data.product_nstp_status &&
  //           item.created_by == item.assigned_to) ||
  //         (item.quote.data.query_opted &&
  //           item.quote.data.query_option_yn == "no")
  //       ) {
  //         item.quote_status = QuoteListingStatus.PENDING;
  //       } else if (
  //         (!item.policy_no &&
  //           item.data &&
  //           item.data.product_nstp_status &&
  //           item.data.product_nstp_status == "Referral") ||
  //         (item.quote.data && item.quote.data.query_option_yn == "no")
  //       ) {
  //         item.quote_status = QuoteListingStatus.UW_REFERRAL;
  //       }
  //     }
  //   });
  // }

  get getList() {
    return this.isFilterApplied ? this.quoteListing : this.quoteListingArray;
  }

  // getPolicies(type: string) {
  //   if (!this.isUnderWriter) {
  //     if (type === QuoteListingFilters.ALL) {
  //       this.clearAllFilter();
  //       this.getQuoteListingData();
  //     } else if (type === QuoteListingFilters.PENDING) {
  //       this.quoteListing = this.getList?.filter(
  //         (item) => item.quote_status === QuoteListingStatus.PENDING
  //       );
  //       this.commonStatus = "Pending Policies";
  //     } else if (type === QuoteListingFilters.APPROVE) {
  //       this.quoteListing = this.getList?.filter(
  //         (quote) =>
  //           quote.quote_status ===
  //           QuoteListingStatus.APPROVED_AND_POLICY_GENERATED
  //       );
  //       this.commonStatus = "Approved and Policy Generated";
  //     } else if (type === QuoteListingFilters.UW) {
  //       this.quoteListing = this.getList?.filter(
  //         (item) => item.quote_status === QuoteListingStatus.UW_REFERRAL
  //       );
  //       this.commonStatus = "UW Policies";
  //     }
  //   } else if (this.isUnderWriter) {
  //     if (type === QuoteListingFilters.ASSIGNED_TO_ME) {
  //       this.uwAssignedGroup = type;
  //       this.getQuoteListingData();
  //     } else if (type === QuoteListingFilters.ASSIGNED_TO_GROUP) {
  //       this.uwAssignedGroup = type;
  //       this.getQuoteListingData();
  //     }
  //   }
  // }

  // filterQuoteList() {
  //   if (!this.quoteListingArray?.length) {
  //     this.getQuoteListingData(true);
  //   } else {
  //     this.filterQuoteListingArray();
  //   }
  // }

  set appliedFilter(value: boolean) {
    this.isFilterApplied = value;
  }

  closeFilterModal() {
    this.sortFilterRef?.hide();
  }
  // filterQuoteListingArray() {
  //   this.quoteListing = this.quoteListing?.length
  //     ? this.quoteListing
  //     : this.quoteListingArray;
  //   if (
  //     this.selectedFilterParameters === QuoteListingFilterParameters.ASCENDING
  //   ) {
  //     this.quoteListing = this.quoteListing?.sort(
  //       (a, b) => new Date(a.u_ts).getTime() - new Date(b.u_ts).getTime()
  //     );
  //   }
  //   if (
  //     this.selectedFilterParameters === QuoteListingFilterParameters.DESCENDING
  //   ) {
  //     this.quoteListing = this.quoteListing?.sort(
  //       (a, b) => new Date(b.u_ts).getTime() - new Date(a.u_ts).getTime()
  //     );
  //   }

  //   if (this.filterFromDate) {
  //     this.quoteListing = this.quoteListing?.filter(
  //       (list) =>
  //         new Date(list.u_ts).getTime() >=
  //         new Date(this.filterFromDate).getTime()
  //     );
  //   }
  //   if (this.filterToDate) {
  //     this.quoteListing = this.quoteListing?.filter(
  //       (list) =>
  //         new Date(list.u_ts).getTime() <= new Date(this.filterToDate).getTime()
  //     );
  //   }
  //   if (this.selectedItemsChannel?.length) {
  //     this.selectedItemsChannel?.forEach((channel) => {
  //       this.quoteListing = this.quoteListing?.filter(
  //         (list) => list?.data?.search?.channel === channel?.name
  //       );
  //     });
  //   }
  //   if (this.selectedItems?.length) {
  //     this.selectedItems?.forEach((location) => {
  //       this.quoteListing = this.quoteListing?.filter(
  //         (list) => list?.data?.search?.channel === location?.name
  //       );
  //     });
  //   }
  //   this.appliedFilter = true;
  //   this.closeFilterModal();
  // }

  clearAllFilter() {
    this.appliedFilter = false;
    this.selectedFilterParameters =
      this.filterToDate =
      this.filterFromDate =
      this.selectedItems =
      this.selectedItemsChannel =
        null;
    this.quoteListing = this.quoteListingArray;
    this.closeFilterModal();
  }

}
