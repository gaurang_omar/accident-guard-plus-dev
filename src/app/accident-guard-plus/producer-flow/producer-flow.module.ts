import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule } from '@ngx-translate/core';
import { QuoteInformationComponent } from './quote-information/quote-information.component';
import { MemberDetailsComponent } from './member-details/member-details.component';
import { AllPremiumsComponent } from './all-premiums/all-premiums.component';
import { PreviewQuoteComponent } from './preview-quote/preview-quote.component';
import { ConvertToProposalComponent } from './convert-to-proposal/convert-to-proposal.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { MemberProposalDetailsComponent } from './member-proposal-details/member-proposal-details.component';
import { NomineeDetailsComponent } from './nominee-details/nominee-details.component';
import { AdditionalPolicyDetailsComponent } from './additional-policy-details/additional-policy-details.component';
import { AdditionalDetailsComponent } from './additional-details/additional-details.component';
import { DocumentUploadComponent } from './document-upload/document-upload.component';
import { ReviewProposalComponent } from './review-proposal/review-proposal.component';
import { QuoteListingComponent } from './quote-listing/quote-listing.component';



@NgModule({
  declarations: [QuoteInformationComponent, MemberDetailsComponent, AllPremiumsComponent, PreviewQuoteComponent, ConvertToProposalComponent, CustomerDetailsComponent, MemberProposalDetailsComponent, NomineeDetailsComponent, AdditionalPolicyDetailsComponent, AdditionalDetailsComponent, DocumentUploadComponent, ReviewProposalComponent, QuoteListingComponent],
  imports: [
    CommonModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      timeOut: 10000,
      preventDuplicates: true
    }),
    TranslateModule
  ]
})
export class ProducerFlowModule { }
