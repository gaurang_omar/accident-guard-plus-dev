import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-document-upload',
  templateUrl: './document-upload.component.html',
  styleUrls: ['./document-upload.component.scss']
})
export class DocumentUploadComponent implements OnInit {

  documentUploadForm : FormGroup;
  dropDownArr = [];

  constructor(private _router : Router,
              private _formBuilder : FormBuilder,
              private cd : ChangeDetectorRef) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm(){
    this.documentUploadForm = this._formBuilder.group({
      documentArr : this._formBuilder.array([this.addDocumentGroup()])
    })
  }

  addDocumentGroup():FormGroup{
    return this._formBuilder.group({
      nameOfTheInsuredPerson: [null],
      documentType: [null],
      document : [''],
      documentName : [''],
      remarks: ['']
    });
  }

  onFileChange(event, documentGroup) {
    let reader = new FileReader();
   
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.setFormFileName(file,documentGroup);
      reader.readAsDataURL(file);
    
      reader.onload = () => {
        this.cd.markForCheck();
      };
    }
  }

  setFormFileName(file,documentGroup){
    documentGroup.get('documentName').setValue(file.name);
  }

  removeDocFile(documentGroup){
    documentGroup.get('documentName').setValue('');
  }

  addDocumentArrBtn(){
    const documentArr = this.documentUploadForm.get('documentArr') as FormArray;
    documentArr.push(this.addDocumentGroup());
  }

  removeDocumentGroupArrBtn(ind){
    const documentArr = this.documentUploadForm.get('documentArr') as FormArray;
    documentArr.removeAt(ind);
  }

  routeBack(){
    this._router.navigate(['accident-guard-plus/additionalDetails']);
  }

  route(){
    this._router.navigate(['accident-guard-plus/previewProposal']);  
  }

  routeBackToProposal(){
      this._router.navigate(['accident-guard-plus/convertToProposal']);  
  }

}
