import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule } from '@ngx-translate/core';
import { AccidentGuardPlusRoutingModule } from './accident-guard-plus-routing.module';
import { ProducerFlowModule } from './producer-flow/producer-flow.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      timeOut: 10000,
      preventDuplicates: true
    }),
    TranslateModule,
    AccidentGuardPlusRoutingModule
  ],
  exports: [
    ProducerFlowModule
  ]
})
export class AccidentGuardPlusModule { }
