import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdditionalDetailsComponent } from './producer-flow/additional-details/additional-details.component';
import { AdditionalPolicyDetailsComponent } from './producer-flow/additional-policy-details/additional-policy-details.component';
import { AllPremiumsComponent } from './producer-flow/all-premiums/all-premiums.component';
import { ConvertToProposalComponent } from './producer-flow/convert-to-proposal/convert-to-proposal.component';
import { CustomerDetailsComponent } from './producer-flow/customer-details/customer-details.component';
import { DocumentUploadComponent } from './producer-flow/document-upload/document-upload.component';
import { MemberDetailsComponent } from './producer-flow/member-details/member-details.component';
import { MemberProposalDetailsComponent } from './producer-flow/member-proposal-details/member-proposal-details.component';
import { NomineeDetailsComponent } from './producer-flow/nominee-details/nominee-details.component';
import { PreviewQuoteComponent } from './producer-flow/preview-quote/preview-quote.component';
import { QuoteInformationComponent } from './producer-flow/quote-information/quote-information.component';
import { QuoteListingComponent } from './producer-flow/quote-listing/quote-listing.component';
import { ReviewProposalComponent } from './producer-flow/review-proposal/review-proposal.component';


const routes: Routes = [
	{ path: 'accident-guard-plus/quoteInformation', component: QuoteInformationComponent},
	{ path: 'accident-guard-plus/memberDetails', component: MemberDetailsComponent},
	{ path: 'accident-guard-plus/allPremiums', component: AllPremiumsComponent},
	{ path: 'accident-guard-plus/previewQuote', component: PreviewQuoteComponent},
	{ path: 'accident-guard-plus/previewProposal', component: ReviewProposalComponent},
	{ path: 'accident-guard-plus/quoteListing', component: QuoteListingComponent},
	{ path: 'accident-guard-plus/convertToProposal', component: ConvertToProposalComponent},
	{ path: 'accident-guard-plus/customerDetails', component: CustomerDetailsComponent},
	{ path: 'accident-guard-plus/memberProposalDetails', component: MemberProposalDetailsComponent},
	{ path: 'accident-guard-plus/nomineeDetails', component: NomineeDetailsComponent},
	{ path: 'accident-guard-plus/additionalPolicyDetails', component: AdditionalPolicyDetailsComponent},
	{ path: 'accident-guard-plus/additionalDetails', component: AdditionalDetailsComponent},
	{ path: 'accident-guard-plus/documentUpload', component: DocumentUploadComponent},
];
@NgModule({
	imports: [RouterModule.forRoot(routes, { useHash: true, scrollPositionRestoration: 'enabled' })],
	exports: [RouterModule]
})
export class AccidentGuardPlusRoutingModule { }
