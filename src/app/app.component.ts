import { Component } from '@angular/core';
import { version } from '../../package.json';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'Tata-health';
  version = version;
  constructor( private translate: TranslateService) {
    // this.apiService.checkLogin();
    console.log(`Verison ${this.version}`);
    translate.addLangs(['en', 'hi']);
    translate.setDefaultLang('en');
  }
  onActivate(event, ref) {
    ref.scrollTop = 0;
    // window.scroll(0,0);
    //or document.body.scrollTop = 0;
    //or document.querySelector('body').scrollTo(0,0)

  }
}
