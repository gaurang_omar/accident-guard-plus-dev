import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-common-notification',
  templateUrl: './common-notification.component.html',
  styleUrls: ['./common-notification.component.sass']
})
export class CommonNotificationComponent implements OnInit {
  notifArr = ['Primary', 'Secondary'];
  productNotification = ['Motor', 'Health', 'Aig Combined', 'Marine', 'Financial Lines', 'Travels'];
  selectedButton = 'Motor';
  productNotifications = {
    'Motor': [{
      label: 'UnderWriter Has Approved Policy motor',
      value: 'Your Proposal',
      br_value: 'has been approved by the underwriter',
      proposal_number: '123456789',
      time: '2 Hours ago',
      button: 'Pay Now',
      flag: false
    },
    {
      label: 'Payment Succesful',
      value: 'Your payment of 10,000 /- for Proposal No : ',
      br_value: 'was successfully completed.Transaction Number :',
      proposal_number: '123456789',
      time: '7 Hours ago',
      button: 'View Policy',
      flag: false

    },
    {
      label: 'UnderWriter Has Approved Policy',
      value: 'Your Proposal',
      br_value: 'has been approved by the underwriter',
      proposal_number: '123456789',
      time: '2 Hours ago',
      button: 'Pay Now',
      flag: false

    },
    {
      label: 'Payment Succesful',
      value: 'Your payment of 10,000 /- for Proposal No : ',
      br_value: 'was successfully completed.Transaction Number :',
      proposal_number: '123456789',
      time: '7 Hours ago',
      button: 'View Policy',
      flag: false

    }],
    'Health': [{
      label: 'UnderWriter Has Approved Policy health',
      value: 'Your Proposal',
      br_value: 'has been approved by the underwriter',
      proposal_number: '123456789',
      time: '2 Hours ago',
      button: 'Pay Now',
      flag: false
    },
    {
      label: 'Payment Succesful',
      value: 'Your payment of 10,000 /- for Proposal No : ',
      br_value: 'was successfully completed.Transaction Number :',
      proposal_number: '123456789',
      time: '7 Hours ago',
      button: 'View Policy',
      flag: false

    },
    {
      label: 'UnderWriter Has Approved Policy',
      value: 'Your Proposal',
      br_value: 'has been approved by the underwriter',
      proposal_number: '123456789',
      time: '2 Hours ago',
      button: 'Pay Now',
      flag: false
    },
    {
      label: 'Payment Succesful',
      value: 'Your payment of 10,000 /- for Proposal No : ',
      br_value: 'was successfully completed.Transaction Number :',
      proposal_number: '123456789',
      time: '7 Hours ago',
      button: 'View Policy',
      flag: false

    }],
    'Aig Combined':[{
      label: 'UnderWriter Has Approved Policy',
      value: 'Your Proposal',
      br_value: 'has been approved by the underwriter',
      proposal_number: '123456789',
      time: '2 Hours ago',
      button: 'Pay Now',
      flag: false
    },

    {
      label: 'Payment Succesful',
      value: 'Your payment of 10,000 /- for Proposal No : ',
      br_value: 'was successfully completed.Transaction Number :',
      proposal_number: '123456789',
      time: '7 Hours ago',
      button: 'View Policy',
      flag: false

    }],
    'Marine':  [{
      label: 'UnderWriter Has Approved Policy',
      value: 'Your Proposal',
      br_value: 'has been approved by the underwriter',
      proposal_number: '123456789',
      time: '2 Hours ago',
      button: 'Pay Now',
      flag: false
    }],
    'Financial Lines':[{
      label: 'UnderWriter Has Approved Policy',
      value: 'Your Proposal',
      br_value: 'has been approved by the underwriter',
      proposal_number: '123456789',
      time: '2 Hours ago',
      button: 'Pay Now',
      flag: false
    },

    {
      label: 'Payment Succesful',
      value: 'Your payment of 10,000 /- for Proposal No : ',
      br_value: 'was successfully completed.Transaction Number :',
      proposal_number: '123456789',
      time: '7 Hours ago',
      button: 'View Policy',
      flag: false

    }],
    'Travels':  [{
      label: 'UnderWriter Has Approved Policy',
      value: 'Your Proposal',
      br_value: 'has been approved by the underwriter',
      proposal_number: '123456789',
      time: '2 Hours ago',
      button: 'Pay Now',
      flag: false
    }]

  }
  product: any;
  constructor() {   
   }

  ngOnInit(): void {
    console.log('notif',this.productNotifications);
    // this.selectedProduct = this.productNotifications['Motor'];

  }
  selectProduct(product){
    // this.selectedProduct = this.productNotifications[product];
    this.selectedButton = product;
    // console.log('selectedProduct----',this.productNotifications[product]);
  }

}
