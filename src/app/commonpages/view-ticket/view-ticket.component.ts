import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-ticket',
  templateUrl: './view-ticket.component.html',
  styleUrls: ['./view-ticket.component.sass'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ViewTicketComponent implements OnInit {
  size = 10;
  isTableExpanded = false;
  TICKET_DATA = [
    {
      "ticketnumber": "Tic001",
      "applicationModule": 'Commercial Lines',
      "status": 'Open',
      "remarks": "Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
      "isExpanded": false,
      "createdDate": '04/06/2021',
      "lastActionDate": '04/06/2021',
      "requestType": 'Production Issues',
      "application": 'IPDS-V2',
      "producerCode": 'xxxxxxxxxx',
      "typeOfDefect": 'General',
      "assignedTo": 'Ipds-V2-L1 Team',
      "attachment": 'Screenshot.JPEG',
      "isResolved": false
    },
    {
      "ticketnumber": "Tic002",
      "applicationModule": 'Commercial Lines',
      "status": 'Resolved',
      "remarks": "Lorem Ipsum",
      "isExpanded": false,
      "createdDate": '04/06/2021',
      "lastActionDate": '04/06/2021',
      "requestType": 'Production Issues',
      "application": 'IPDS-V2',
      "producerCode": 'xxxxxxxxxx',
      "typeOfDefect": 'General',
      "assignedTo": 'Ipds-V2-L1 Team',
      "attachment": 'Screenshot.JPEG',
      "isResolved": true
    },
    {
      "ticketnumber": "Tic003",
      "applicationModule": 'Commercial Lines',
      "status": 'Resolved',
      "remarks": "Lorem Ipsum",
      "isExpanded": false,
      "createdDate": '04/06/2021',
      "lastActionDate": '04/06/2021',
      "requestType": 'Production Issues',
      "application": 'IPDS-V2',
      "producerCode": 'xxxxxxxxxx',
      "typeOfDefect": 'General',
      "assignedTo": 'Ipds-V2-L1 Team',
      "attachment": 'Screenshot.JPEG',
      "isResolved": true
    },
    {
      "ticketnumber": "Tic004",
      "applicationModule": 'Commercial Lines',
      "status": 'Open',
      "remarks": "Lorem Ipsum",
      "isExpanded": false,
      "createdDate": '04/06/2021',
      "lastActionDate": '04/06/2021',
      "requestType": 'Production Issues',
      "application": 'IPDS-V2',
      "producerCode": 'xxxxxxxxxx',
      "typeOfDefect": 'General',
      "assignedTo": 'Ipds-V2-L1 Team',
      "attachment": 'Screenshot.JPEG',
      "isResolved": false
    },
    {
      "ticketnumber": "Tic005",
      "applicationModule": 'Commercial Lines',
      "status": 'Open',
      "remarks": "Lorem Ipsum",
      "isExpanded": false,
      "createdDate": '04/06/2021',
      "lastActionDate": '04/06/2021',
      "requestType": 'Production Issues',
      "application": 'IPDS-V2',
      "producerCode": 'xxxxxxxxxx',
      "typeOfDefect": 'General',
      "assignedTo": 'Ipds-V2-L1 Team',
      "attachment": 'Screenshot.JPEG',
      "isResolved": false
    },
    {
      "ticketnumber": "Tic006",
      "applicationModule": 'Commercial Lines',
      "status": 'Resolved',
      "remarks": "Lorem Ipsum",
      "isExpanded": false,
      "createdDate": '04/06/2021',
      "lastActionDate": '04/06/2021',
      "requestType": 'Production Issues',
      "application": 'IPDS-V2',
      "producerCode": 'xxxxxxxxxx',
      "typeOfDefect": 'General',
      "assignedTo": 'Ipds-V2-L1 Team',
      "attachment": 'Screenshot.JPEG',
      "isResolved": true
    },
    {
      "ticketnumber": "Tic007",
      "applicationModule": 'Commercial Lines',
      "status": 'Resolved',
      "remarks": "Lorem Ipsum",
      "isExpanded": false,
      "createdDate": '04/06/2021',
      "lastActionDate": '04/06/2021',
      "requestType": 'Production Issues',
      "application": 'IPDS-V2',
      "producerCode": 'xxxxxxxxxx',
      "typeOfDefect": 'General',
      "assignedTo": 'Ipds-V2-L1 Team',
      "attachment": 'Screenshot.JPEG',
      "isResolved": true
    },
    {
      "ticketnumber": "Tic008",
      "applicationModule": 'Commercial Lines',
      "status": 'Open',
      "remarks": "Lorem Ipsum",
      "isExpanded": false,
      "createdDate": '04/06/2021',
      "lastActionDate": '04/06/2021',
      "requestType": 'Production Issues',
      "application": 'IPDS-V2',
      "producerCode": 'xxxxxxxxxx',
      "typeOfDefect": 'General',
      "assignedTo": 'Ipds-V2-L1 Team',
      "attachment": 'Screenshot.JPEG',
      "isResolved": false
    },
    {
      "ticketnumber": "Tic009",
      "applicationModule": 'Commercial Lines',
      "status": 'Open',
      "remarks": "Lorem Ipsum",
      "isExpanded": false,
      "createdDate": '04/06/2021',
      "lastActionDate": '04/06/2021',
      "requestType": 'Production Issues',
      "application": 'IPDS-V2',
      "producerCode": 'xxxxxxxxxx',
      "typeOfDefect": 'General',
      "assignedTo": 'Ipds-V2-L1 Team',
      "attachment": 'Screenshot.JPEG',
      "isResolved": false
    },
    {
      "ticketnumber": "Tic010",
      "applicationModule": 'Commercial Lines',
      "status": 'Resolved',
      "remarks": "Lorem Ipsum",
      "isExpanded": false,
      "createdDate": '04/06/2021',
      "lastActionDate": '04/06/2021',
      "requestType": 'Production Issues',
      "application": 'IPDS-V2',
      "producerCode": 'xxxxxxxxxx',
      "typeOfDefect": 'General',
      "assignedTo": 'Ipds-V2-L1 Team',
      "attachment": 'Screenshot.JPEG',
      "isResolved": true
    },
    {
      "ticketnumber": "Tic011",
      "applicationModule": 'Commercial Lines',
      "status": 'Resolved',
      "remarks": "Lorem Ipsum",
      "isExpanded": false,
      "createdDate": '04/06/2021',
      "lastActionDate": '04/06/2021',
      "requestType": 'Production Issues',
      "application": 'IPDS-V2',
      "producerCode": 'xxxxxxxxxx',
      "typeOfDefect": 'General',
      "assignedTo": 'Ipds-V2-L1 Team',
      "attachment": 'Screenshot.JPEG',
      "isResolved": true
    },
    {
      "ticketnumber": "Tic012",
      "applicationModule": 'Commercial Lines',
      "status": 'Open',
      "remarks": "Lorem Ipsum",
      "isExpanded": false,
      "createdDate": '04/06/2021',
      "lastActionDate": '04/06/2021',
      "requestType": 'Production Issues',
      "application": 'IPDS-V2',
      "producerCode": 'xxxxxxxxxx',
      "typeOfDefect": 'General',
      "assignedTo": 'Ipds-V2-L1 Team',
      "attachment": 'Screenshot.JPEG',
      "isResolved": false
    }
  ];
  page: number = 1;
  dataTicketList: any = new MatTableDataSource();
  headerList: string[] = ['ticketnumber', 'applicationModule', 'status', 'createdDate', 'lastActionDate', 'assignedTo', 'actions'];
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.dataTicketList.data = this.TICKET_DATA;
    this.dataTicketList = this.TICKET_DATA.slice(0, this.size);
  }

  /**
   * Function which will help us to paginate the table
   * @param event : Event
   */
  paginate(event: any) {
    this.page = event;
    this.dataTicketList = this.TICKET_DATA.slice(event * this.size - this.size, event * this.size);
  }

  /**
   * FUnction to Route back to previous page
   */
  routeBack() {
    this.router.navigate(['helpTicket']);
  }

  /**
   * Collapse Other rows of table
   * @param element : Clicked row 
   */
  collapseAll(element) {
    this.TICKET_DATA.forEach((el: any) => {
      if (el.ticketnumber != element.ticketnumber)
        el.isExpanded = false;
    })
  }
}
