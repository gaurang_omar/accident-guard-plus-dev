import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuoteInformationComponent } from './accident-guard-plus/producer-flow/quote-information/quote-information.component';


const routes: Routes = [
	{ path: '', redirectTo: 'accident-guard-plus/quoteInformation', pathMatch: 'full' },
	{ path: 'accident-guard-plus/quoteInformation', component: QuoteInformationComponent}
];
@NgModule({
	imports: [RouterModule.forRoot(routes, { useHash: true, scrollPositionRestoration: 'enabled' })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
